const fs = require('fs');

let tribute = require('../tributes/Tribute.js');
let maleNames = require('../tributes/names/male.js');
let femaleNames = require('../tributes/names/female.js');
let Tribute = tribute.Tribute;
let tributes = [];

startGame = () => {
	for(var i = 1; i < 4; i++){
		let f = Math.floor(Math.random() * femaleNames.length);
		let fn = femaleNames[f];
		femaleNames.splice(f, 1);
		let m = Math.floor(Math.random() * maleNames.length)
		let mn = maleNames[m];
		maleNames.splice(m, 1);
		tributes.push(new Tribute(mn, i, "male"));
		tributes.push(new Tribute(fn, i, "female"));
    }
    let msg = "List of tributes:";
	for(var i = 0; i < tributes.length; i++){
		msg += "\n\n" + tributes[i].toString();
	}
    fs.writeFile('./currentTributes.json', JSON.stringify(tributes), () => {
        console.log(msg);
    })
}
startGame();
module.exports = startGame;