femaleNames = ["Katniss", "Rue", "Prim", "Rose", "Rainbowdash", "Crystal"];
femaleNames.push("Glimmer", "Twilight", "Violet", "Clove", "Foxface", "Claudia");
femaleNames.push("Sparkles", "Applejack", "Fluttershy", "Pinkie", "Venia", "Octavia");
femaleNames.push("Annie", "Cashmere", "Gloss", "Enobaria", "Wiress", "Mags")
femaleNames.push("Rarity", "Johanna", "Leceilia", "Ceceilia",  "Seeder", "Cressida");
femaleNames.push("Bonnie", "Paylor", "Delly", "Maysilee", "Messala");

module.exports = femaleNames;