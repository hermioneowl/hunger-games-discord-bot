maleNames = ["Peeta", "Gale", "Crane", "Cinna", "Cato", "Alpha"];
maleNames.push("Caesar", "Marvel", "Thresh", "Hob", "Claudius", "Flavius");
maleNames.push("Julius", "Brutus", "Hawthorne", "Sae", "Finnick", "BeeTee");
maleNames.push("Blight", "Woof", "Crest", "Chaff", "Castor", "Pollux");
maleNames.push("Serius", "Twill", "Romulus", "Darius", "Remus", "Jackson");

module.exports = maleNames;