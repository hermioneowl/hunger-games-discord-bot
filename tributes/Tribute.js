class Tribute{
    
    constructor (name, district, gender){
        this.name = name;
        this.district = district;
        this.gender = gender;
        this.stat = Math.floor(Math.random() * Math.round((6+((13-this.district)*2)))) + 1;
        this.price = 50+(this.stat*2);
        this.health = 100;
        this.sponsor = "";
        this.handInv = [];
        this.backpack = false;
        this.backInv = [];
        this.kills = [];
    }
    // constructor (name, district, gender, stat, price){
    //     this.name = name;
    //     this.district = district;
    //     this.gender = gender;
    //     this.stat = stat;
    //     this.price = price;
    //     this.health = 100;
    //     this.sponsor = "";
    //     this.handInv = [];
    //     this.backpack = false;
    //     this.backInv = [];
    //     this.kills = [];
    // }
    
    grabsBkpk() {
        backpack = true;
    }
    wounded(wound){
        if(wound > 60){
            health = 0;
        } else{
            health -= wound;
        }
    }
    eat(n){
        health+=n;
        if(health > 100){
            health = 100;
        }
    }
    assign(sponsor){
        this.sponsor = sponsor;
    }
    toString(){
        return this.name + ": District " + this.district + " " + this.gender + "\nEpicness: " + this.stat + "\nPrice: " + this.price;
    }
}
exports.Tribute = Tribute;