class Player{
    
    constructor (username){
        this.username = username;
        this.tributes = [];
        this.balance = 1000;
        this.continue = false;
    }
    assign(tribute){
        if(tribute.sponsor != ""){
            this.tributes.push(tribute)
            this.balance-= tribute.price;
            tribute.assign(this.username);
        }
    }
    continueTrue(){
        this.continue = true;
    }
    continueFalse(){
        this.continue = false;
    }
    get balance(){
        return this.balance;
    }
    toString(){
        return this.username;
    }
}
module.exports = Player;