module.exports = {
	name: 'choose',
	description: 'Gives a certain tribute to a user',
	aliases: ['ch'],
	usage: 'choose [tribute]',
	execute(message, args) {
		if(args.length < 1){
			message.reply("Please provide a tribute as argument");
		} else{
			message.channel.send('I would be giving you the tribute ' + args[0] + ' if the code worked.');
		}
	},
};