module.exports = {
	name: 'quit',
	description: 'User no longer participating in current Hunger Games',
	aliases: ['q', 'done','ugh', 'thisisstupid'],
	execute(message) {
		message.channel.send('Goodbye');
	},
};