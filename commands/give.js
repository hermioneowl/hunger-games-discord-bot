module.exports = {
	name: 'give',
	description: 'Gives tributes objects for their inventory.',
	usage: 'give [tribute] [object]',
	aliases: ['g', 'send', 'parachute'],
	execute(message, args) {
		if(args.length < 2){
			message.reply("Please put it in this format: give [tribute] [object]. Thank you!");
		} else{
			message.channel.send('I would be giving ' + args[0] + ' a ' + args[1] + ' but the code has\'nt been created');
		}	
	},
};