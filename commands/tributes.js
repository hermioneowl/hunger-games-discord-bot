const tributes = require('../game/currentTributes.json');

module.exports = {
	name: 'tributes',
	description: 'Shows all of the tributes',
	aliases: ['t', 'trib'],
	execute(message) {
        message.channel.send('Tributes: \n');
        for(var i = 0; i < tributes.length; i++){
            let msg = "";
            msg +=  "**" + tributes[i]["name"] + "**: District " + tributes[i]["district"] + " " + tributes[i]["gender"] + "\n";
            msg += "Epicness: " + tributes[i]["stat"] + "\n";
            msg += "Price: " + tributes[i]["price"] + "\n";
            message.channel.send(msg);
        }
	},
};