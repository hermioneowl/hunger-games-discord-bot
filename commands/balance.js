module.exports = {
	name: 'balance',
	description: 'Shows a player their balance',
	aliases: ['b', 'ba', 'money', 'bank'],
	execute(message) {
		message.reply('Your balance: -10000000');
	},
};