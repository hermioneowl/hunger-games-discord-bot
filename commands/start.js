// const startGame = require('../game/startGame.js');
const fs = require('fs');
let tribute = require('../tributes/Tribute.js');
let maleNames = require('../tributes/names/male.js');
let femaleNames = require('../tributes/names/female.js');
// const { start } = require('repl');
let Tribute = tribute.Tribute;
let tributes = [];

startGame = () => {
	for(var i = 1; i < 4; i++){
		let f = Math.floor(Math.random() * femaleNames.length);
		let fn = femaleNames[f];
		femaleNames.splice(f, 1);
		let m = Math.floor(Math.random() * maleNames.length)
		let mn = maleNames[m];
		maleNames.splice(m, 1);
		tributes.push(new Tribute(mn, i, "male"));
		tributes.push(new Tribute(fn, i, "female"));
    }
    let msg = "List of tributes:";
	for(var i = 0; i < tributes.length; i++){
		msg += "\n\n" + tributes[i].toString();
	}
    fs.writeFile('./games/currentTributes.json', JSON.stringify(tributes), () => {
        console.log(msg);
    })
}


module.exports = {
	name: 'start',
	description: 'Starts a new game',
	aliases: ['st', 'begin', 'open', 'imbored'],
	execute(message) {
		startGame();
		message.channel.send("Starting game. To join do `~join`. To see the current tributes, do `~tributes`. ")
		// var tributes = [];
		// for(var i = 1; i < 13; i++){
		// 	let f = Math.floor(Math.random() * femaleNames.length);
		// 	let fn = femaleNames[f];
		// 	femaleNames.splice(f, 1);
		// 	let m = Math.floor(Math.random() * maleNames.length)
		// 	let mn = maleNames[m];
		// 	maleNames.splice(m, 1);
		// 	tributes.push(new Tribute(mn, i, "male"));
		// 	tributes.push(new Tribute(fn, i, "female"));
		// }
		// let msg = "I would be starting a game if the code worked. However, I can send you this list of tributes I just created:";
		// for(var i = 0; i < tributes.length; i++){
		// 	msg += "\n\n" + tributes[i].toString();
		// }
		// console.log(msg);
		// message.channel.send(msg);
	},
};