module.exports = {
	name: 'continue',
	description: 'Continues onto the next round',
	aliases: ['co', 'cont', 'round', 'next'],
	execute(message) {
		message.channel.send('Continuing the nonexistent game');
	},
};