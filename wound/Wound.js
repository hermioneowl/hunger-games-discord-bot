class Wound{
    constructor(type, minDam, maxDam){
        this.type = type;
        this.minDam = minDam;
        this.maxDam = maxDam;
    }
    heal(factor){
        this.minDam -= factor;
        this.maxDam -= factor;
    }
}

module.exports = Wound;