let thing = require('./Thing.js');
let Thing = thing.Thing;

class Weapon extends Thing{
    constructor(name, price, minDam, maxDam){
        super(name, price);
        this.minDam = minDam;
        this.maxDam = maxDam;
    }
}

module.exports = Weapon;