let thing = require('./Thing.js');
let Thing = thing.Thing;

class Food extends Thing{
    constructor(name, price, healing){
        super(name, price);
        this.heals = healing;
    }
}

module.exports = Food;